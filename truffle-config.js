const HDWalletProvider = require('truffle-hdwallet-provider');
require('dotenv').config();

module.exports = {
  networks: {
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*',
      gas: 6000000,
      gasLimit: 6000000,
      gasPrice: 1,
    },
    ropsten: {
      provider: () => new HDWalletProvider(
        process.env.MNENOMIC,
        `https://ropsten.infura.io/v3/${process.env.INFURA_API_KEY}`,
      ),
      network_id: 3,
      // gasLimit: 8000000,
      gasPrice: 15000000000,
    },
    rinkeby: {
      provider: () => new HDWalletProvider(
        process.env.MNENOMIC,
        `https://rinkeby.infura.io/v3/${process.env.INFURA_API_KEY}`,
      ),
      network_id: 4,
      gasPrice: 5000000000,
    },
    kovan: {
      provider: () => new HDWalletProvider(
        process.env.MNENOMIC,
        `https://kovan.infura.io/v3/${process.env.INFURA_API_KEY}`, 0, 3,
      ),
      network_id: 42,
      // gasLimit: 8000000,
      gasPrice: 10000000,
    },
    bscTestnet: {
      provider: () => new HDWalletProvider(process.env.MNENOMIC, 'https://data-seed-prebsc-1-s1.binance.org:8545'),
      network_id: 97,
      confirmations: 10,
      timeoutBlocks: 200,
      skipDryRun: true,
      gasPrice: 10000000000,
    },
    bsc: {
      provider: () => new HDWalletProvider(process.env.MNENOMIC, 'https://bsc-dataseed1.binance.org'),
      network_id: 56,
      confirmations: 10,
      timeoutBlocks: 200,
      skipDryRun: true,
      gasPrice: 5000000000,
    },
    main: {
      provider: () => new HDWalletProvider(
        process.env.MNENOMIC, `https://mainnet.infura.io/v3/${process.env.INFURA_API_KEY}`,
      ),
      gasPrice: 1,
      network_id: 1,
    },
  },

  compilers: {
    solc: {
      version: '0.8.4',
      optimizer: {
        enabled: false,
        runs: 200,
      },
    },
  },

  api_keys: {
    etherscan: process.env.ETHERSCAN_API_KEY,
    bscscan: process.env.BSCSCAN_API_KEY,
  },

  plugins: [
    'truffle-plugin-solhint',
    'truffle-plugin-verify',
  ],
};
