/* eslint-disable max-len */
const BN = require('../helpers/bn');

class Mock {
  constructor() {
    this.bn = new BN();
    this.poolTypes = { PROPOSAL: '0', EXECUTIVE: '1', EVENT: '2', PRIVATE: '3' };
    this.poolStatus = { PENDING: '0', APPROVED: '1', REJECTED: '2', DRAW: '3' };
    this.voteTypes = { FOR: '0', AGAINST: '1' };
  }

  getProposalPolls(currentTime) {
    return [
      { pollDeadline: Number(currentTime) + 10, type: this.poolTypes.PROPOSAL, pollInfo: '' },
      { pollDeadline: Number(currentTime) + 10000, type: this.poolTypes.PROPOSAL, pollInfo: 'Short!' },
      { pollDeadline: Number(currentTime) + 1000000, type: this.poolTypes.PROPOSAL, pollInfo: 'Long! Long! Long! Long! Long! Long!' },
    ];
  }

  getExecutivePolls(currentTime) {
    return [
      { pollDeadline: Number(currentTime) + 10, type: this.poolTypes.EXECUTIVE, pollInfo: '' },
      { pollDeadline: Number(currentTime) + 1000, type: this.poolTypes.EXECUTIVE, pollInfo: 'Short!' },
      { pollDeadline: Number(currentTime) + 1000000, type: this.poolTypes.EXECUTIVE, pollInfo: 'Long! Long! Long! Long! Long! Long!' },
    ];
  }

  getEventPolls(currentTime) {
    return [
      { pollDeadline: Number(currentTime) + 10, type: this.poolTypes.EVENT, pollInfo: '' },
      { pollDeadline: Number(currentTime) + 1000, type: this.poolTypes.EVENT, pollInfo: 'Short!' },
      { pollDeadline: Number(currentTime) + 1000000, type: this.poolTypes.EVENT, pollInfo: 'Long! Long! Long! Long! Long! Long!' },
    ];
  }

  getPrivatePoll(currentTime, address1, address2, address3) {
    return [
      { pollDeadline: Number(currentTime) + 10, type: this.poolTypes.PRIVATE, pollInfo: '', verifiedAddresses: [address1, address2, address3] },
      { pollDeadline: Number(currentTime) + 1000, type: this.poolTypes.PRIVATE, pollInfo: 'Short!', verifiedAddresses: [address1, address2] },
      { pollDeadline: Number(currentTime) + 1000000, type: this.poolTypes.PRIVATE, pollInfo: 'Long! Long! Long! Long! Long! Long!', verifiedAddresses: [address1, address2] },
    ];
  }

  getAllPolls(currentTime, address1, address2, address3) {
    return (this.getProposalPolls(currentTime)).concat(this.getExecutivePolls(currentTime))
      .concat(this.getEventPolls(currentTime)).concat(this.getPrivatePoll(currentTime, address1, address2, address3));
  }

  getVotesTotalFor(address1, address2) {
    return [
      { voteType: this.voteTypes.FOR, voteWeight: this.bn.toWei('2'), from: address1 },
      { voteType: this.voteTypes.FOR, voteWeight: this.bn.toWei('5'), from: address1 },
      { voteType: this.voteTypes.FOR, voteWeight: this.bn.toWei('3'), from: address1 },
      { voteType: this.voteTypes.AGAINST, voteWeight: this.bn.toWei('1'), from: address2 },
      { voteType: this.voteTypes.AGAINST, voteWeight: this.bn.toWei('2'), from: address2 },
    ];
  }

  getVotesTotalAgainst(address1, address2) {
    return [
      { voteType: this.voteTypes.FOR, voteWeight: this.bn.toWei('2'), from: address1 },
      { voteType: this.voteTypes.FOR, voteWeight: this.bn.toWei('5'), from: address1 },
      { voteType: this.voteTypes.FOR, voteWeight: this.bn.toWei('3'), from: address1 },
      { voteType: this.voteTypes.AGAINST, voteWeight: this.bn.toWei('7'), from: address2 },
      { voteType: this.voteTypes.AGAINST, voteWeight: this.bn.toWei('8'), from: address2 },
    ];
  }

  getVotesTotalDraw(address1, address2) {
    return [
      { voteType: this.voteTypes.FOR, voteWeight: this.bn.toWei('2'), from: address1 },
      { voteType: this.voteTypes.FOR, voteWeight: this.bn.toWei('5'), from: address1 },
      { voteType: this.voteTypes.FOR, voteWeight: this.bn.toWei('3'), from: address1 },
      { voteType: this.voteTypes.AGAINST, voteWeight: this.bn.toWei('6'), from: address2 },
      { voteType: this.voteTypes.AGAINST, voteWeight: this.bn.toWei('4'), from: address2 },
    ];
  }


  getVotes2() {
    // Total weight 20
    return [
      { voteType: this.voteTypes.FOR, voteWeight: this.bn.toWei('3') },
      { voteType: this.voteTypes.AGAINST, voteWeight: this.bn.toWei('6') },
      { voteType: this.voteTypes.FOR, voteWeight: this.bn.toWei('2') },
      { voteType: this.voteTypes.AGAINST, voteWeight: this.bn.toWei('8') },
      { voteType: this.voteTypes.FOR, voteWeight: this.bn.toWei('1') },
    ];
  }

  calculateVoteWeightByVotes(votes) {
    let approveWeight = this.bn.toBN('0');
    let rejectWeight = this.bn.toBN('0');

    for (let i = 0; i < votes.length; i++) {
      if (votes[i].voteType === this.voteTypes.FOR) {
        approveWeight = approveWeight.plus(votes[i].voteWeight);
      } else {
        rejectWeight = rejectWeight.plus(votes[i].voteWeight);
      }
    }

    const totalWeight = approveWeight.plus(rejectWeight);

    return { approveWeight, rejectWeight, totalWeight };
  }
}

module.exports = Mock;
