/* eslint-disable max-len */
const Mock = require('../mocks/mock');
const setCurrentTime = require('./truffle/ganacheTimeTraveler');

class Helper {
  constructor(cvnx, cvnxGovernance) {
    this.cvnx = cvnx;
    this.cvnxGovernance = cvnxGovernance;
    this.mock = new Mock();
  }

  solPollToJsPoll(solObj) {
    return {
      pollDeadline: solObj.pollDeadline.toNumber(),
      pollStopped: solObj.pollStopped.toNumber(),
      pollType: solObj.pollType.toString(),
      pollOwner: solObj.pollOwner,
      forWeight: solObj.forWeight.toString(),
      againstWeight: solObj.againstWeight.toString(),
      pollInfo: solObj.pollInfo,
    };
  }

  solVoteToJsVote(solObj) {
    return {
      voteType: solObj.voteType.toNumber(),
      voteWeight: solObj.voteWeight.toString(),
    };
  }

  async createAllPolls(currentTime, address1, address2, address3) {
    const polls = this.mock.getAllPolls(currentTime, address1, address2, address3);

    for (let i = 0; i < polls.length; i++) {
      await setCurrentTime(currentTime);

      switch (polls[i].type) {
        case this.mock.poolTypes.PROPOSAL:
          await this.cvnxGovernance.createProposalPoll(polls[i].pollDeadline, polls[i].pollInfo);
          break;
        case this.mock.poolTypes.EXECUTIVE:
          await this.cvnxGovernance.createExecutivePoll(polls[i].pollDeadline, polls[i].pollInfo);
          break;
        case this.mock.poolTypes.EVENT:
          await this.cvnxGovernance.createEventPoll(polls[i].pollDeadline, polls[i].pollInfo);
          break;
        case this.mock.poolTypes.PRIVATE:
          await this.cvnxGovernance.createPrivatePoll(polls[i].pollDeadline, polls[i].pollInfo, polls[i].verifiedAddresses);
          break;
      }
    }

    return polls;
  }
}

module.exports = Helper;
