/* eslint-disable max-len */
const { use, expect } = require('chai');
const { solidity } = require('ethereum-waffle');
const truffleAssert = require('truffle-assertions');

use(solidity);
use(require('chai-as-promised')).should();

const CVNX = artifacts.require('CVNX');
const CVNXGovernance = artifacts.require('CVNXGovernance');

const Helper = require('./helpers/helper');
const Reverter = require('./helpers/truffle/reverter');
const setCurrentTime = require('./helpers/truffle/ganacheTimeTraveler');

const Mock = require('./mocks/mock');

contract('CVNX', async (accounts) => {
  const reverter = new Reverter(web3);
  const mock = new Mock();
  let h;

  let cvnx;
  let cvnxGovernance;

  const OWNER = accounts[0];
  const IVAN = accounts[1];
  const OLEG = accounts[2];
  const OLGA = accounts[3];

  const currentTime = Math.round((new Date().getTime()) / 1000);

  before(async () => {
    cvnx = await CVNX.new();
    const cvnxGovernanceAddress = await cvnx.cvnxGovernanceContract();
    cvnxGovernance = await CVNXGovernance.at(cvnxGovernanceAddress);

    h = new Helper(cvnx, cvnxGovernance);

    await reverter.snapshot();
  });

  afterEach(async () => {
    await reverter.revert();
  });

  describe('createPoll()', async () => {
    it('should create poll', async () => {
      const polls = await h.createAllPolls(currentTime, OLEG, IVAN, accounts[5]);

      for (let pollNum = 0; pollNum < polls.length; pollNum++) {
        const poll = h.solPollToJsPoll(await cvnxGovernance.polls(pollNum));

        assert.equal(poll.pollDeadline, polls[pollNum].pollDeadline);
        assert.equal(poll.pollStopped, polls[pollNum].pollDeadline);
        assert.equal(poll.pollOwner, OWNER);
        assert.equal(poll.pollType, polls[pollNum].type);
        assert.equal(poll.forWeight, 0);
        assert.equal(poll.againstWeight, 0);
        assert.equal(poll.pollInfo, polls[pollNum].pollInfo);

        if (polls[pollNum].type === mock.poolTypes.PRIVATE) {
          const { verifiedAddresses } = polls[pollNum];

          for (let k = 0; k < verifiedAddresses.length; k++) {
            const isAddressVerified = await cvnxGovernance.verifiedToVote(pollNum, verifiedAddresses[k]);
            assert.equal(isAddressVerified, true);
          }
        }
      }
    });

    it('should revert if account balance is zero', async () => {
      await setCurrentTime(currentTime);
      await expect(cvnxGovernance.createProposalPoll(currentTime + 600, '', { from: IVAN }))
        .to.be.revertedWith('[E-34] - Your balance is too low.');
    });

    it('should revert if poll deadline equal current timestamp', async () => {
      await expect(cvnxGovernance.createProposalPoll(currentTime, ''))
        .to.be.revertedWith('[E-41] - The deadline must be longer than the current time.');
    });

    it('should revert if caller not a contract owner', async () => {
      await expect(cvnxGovernance.createExecutivePoll(currentTime + 600, '', { from: IVAN }))
        .to.be.revertedWith('Ownable: caller is not the owner');

      await expect(cvnxGovernance.createEventPoll(currentTime + 600, '', { from: IVAN }))
        .to.be.revertedWith('Ownable: caller is not the owner');

      await expect(cvnxGovernance.createPrivatePoll(currentTime + 600, '', [OLEG], { from: IVAN }))
        .to.be.revertedWith('Ownable: caller is not the owner');
    });

    it('should revert if valid addresses length less then 1 in PRIVATE poll', async () => {
      await expect(cvnxGovernance.createPrivatePoll(currentTime + 600, '', [OLEG]))
        .to.be.revertedWith('[E-35] - Verified addresses not set.');
    });
  });

  describe('vote()', async () => {
    beforeEach(async () => {
      const baseBalance = mock.bn.toWei('200');
      await cvnx.transfer(IVAN, baseBalance);
      await cvnx.transfer(OLEG, baseBalance);
    });

    it('should vote in pull', async () => {
      const polls = await h.createAllPolls(currentTime, OLEG, IVAN, accounts[5]);
      const votes = mock.getVotesTotalFor(OLEG, IVAN);

      for (let pollNum = 0; pollNum < polls.length; pollNum++) {
        let forWeight = mock.bn.toBN('0');
        let againstWeight = mock.bn.toBN('0');

        for (let i = 0; i < votes.length; i++) {
          const lockedBefore = (await cvnx.lockedAmount(votes[i].from)).toString();

          await setCurrentTime(currentTime);
          await cvnxGovernance.vote(pollNum, votes[i].voteType, votes[i].voteWeight, { from: votes[i].from });

          const lockedAfter = (await cvnx.lockedAmount(votes[i].from)).toString();

          assert.equal(lockedAfter, mock.bn.toBN(lockedBefore).plus(votes[i].voteWeight).toString());

          const isTokenLockedInPoll = await cvnxGovernance.isTokenLockedInPoll(pollNum, votes[i].from);
          assert.equal(isTokenLockedInPoll, true);

          if (votes[i].voteType === mock.voteTypes.FOR) forWeight = forWeight.plus(votes[i].voteWeight);
          else againstWeight = againstWeight.plus(votes[i].voteWeight);

          const vote = h.solVoteToJsVote(await cvnxGovernance.voted(pollNum, votes[i].from));
          assert.equal(vote.voteType, votes[i].voteType);

          const poll = h.solPollToJsPoll(await cvnxGovernance.polls(pollNum));
          assert.equal(poll.forWeight, forWeight.toString());
          assert.equal(poll.againstWeight, againstWeight.toString());
        }
      }
    });

    it('should emit event PollVote and TokenLock', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, '', { from: OLEG }); // Poll 0

      const voteWeight = '333';
      const voteTransaction = await cvnxGovernance.vote(0, mock.voteTypes.FOR, voteWeight, { from: IVAN });

      truffleAssert.eventEmitted(voteTransaction, 'PollVote', async (event) => {
        assert.equal(event.voterAddress, IVAN);
        assert.equal(event.voteType.toString(), mock.voteTypes.FOR);
        assert.equal(event.voteWeight.toString(), voteWeight);
        return true;
      });

      const oldEvent = await cvnx.getPastEvents('TokenLock', { fromBlock: 0, toBlock: 'latest' });
      assert.equal(oldEvent[0].args.tokenOwner, IVAN);
      assert.equal(oldEvent[0].args.amount.toString(), voteWeight);
    });

    it('should revert if poll end', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await setCurrentTime(currentTime + 600);
      await expect(cvnxGovernance.vote(0, mock.voteTypes.FOR, '1')).to.be.revertedWith('[E-37] - Poll ended.');
    });

    it('should revert if invalid address in PRIVATE poll', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createPrivatePoll(currentTime + 600, '', [IVAN, OLEG]); // Poll 0

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '1', { from: OLEG });
      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '1', { from: IVAN });

      await expect(cvnxGovernance.vote(0, mock.voteTypes.FOR, '1'))
        .to.be.revertedWith('[E-38] - You are not verify to vote in this poll.');
    });

    it('should revert if try to vote FOR and AGAINST in one poll', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '1', { from: OLEG });

      await expect(cvnxGovernance.vote(0, mock.voteTypes.AGAINST, '1', { from: OLEG }))
        .to.be.revertedWith('[E-39] - The voice type does not match the first one.');
    });

    it('should revert if sender balance less or equal 10CVNX', async () => {
      await cvnx.transfer(OLGA, mock.bn.toWei('10'));

      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await expect(cvnxGovernance.vote(0, mock.voteTypes.FOR, '1', { from: OLGA }))
        .to.be.revertedWith('[E-34] - Your balance is too low.');
    });
  });

  describe('unlockTokensInPoll()', async () => {
    beforeEach(async () => {
      const baseBalance = mock.bn.toWei('200');
      await cvnx.transfer(IVAN, baseBalance);
      await cvnx.transfer(OLEG, baseBalance);
    });

    it('should unlock tokens when user vote few time and unlock one by one', async () => {
      const polls = await h.createAllPolls(currentTime, OLEG, IVAN, accounts[5]);
      const voters = [OLEG, IVAN];
      const votes = mock.getVotesTotalAgainst(voters[0], voters[1]);

      // Vote in poll
      for (let pollNum = 0; pollNum < polls.length; pollNum++) {
        for (let i = 0; i < votes.length; i++) {
          await setCurrentTime(currentTime);
          await cvnxGovernance.vote(pollNum, votes[i].voteType, votes[i].voteWeight, { from: votes[i].from });
        }
      }

      // Unlock tokens
      for (let pollNum = 0; pollNum < polls.length; pollNum++) {
        for (let voterIndex = 0; voterIndex < voters.length; voterIndex++) {
          const lockedBefore = (await cvnx.lockedAmount(voters[voterIndex])).toString();

          await setCurrentTime(currentTime + 1000000);
          await cvnxGovernance.unlockTokensInPoll(pollNum, { from: voters[voterIndex] });

          const isTokenLockedInPoll = await cvnxGovernance.isTokenLockedInPoll(pollNum, voters[voterIndex]);
          assert.equal(isTokenLockedInPoll, false);

          const lockedAfter = (await cvnx.lockedAmount(voters[voterIndex])).toString();
          const voterVoteInPoll = h.solVoteToJsVote(await cvnxGovernance.voted(pollNum, voters[voterIndex]));

          assert.equal(lockedAfter, mock.bn.toBN(lockedBefore).minus(voterVoteInPoll.voteWeight).toString());
        }
      }

      for (let voterIndex = 0; voterIndex < voters.length; voterIndex++) {
        const lockedAtEnd = (await cvnx.lockedAmount(voters[voterIndex])).toString();
        assert.equal(lockedAtEnd, '0');
      }
    });

    it('should revert if try to unlock tokens few time in the same poll', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '1', { from: OLEG });

      await setCurrentTime(currentTime + 600);
      await cvnxGovernance.unlockTokensInPoll(0, { from: OLEG });

      await expect(cvnxGovernance.unlockTokensInPoll(0, { from: OLEG }))
        .to.be.revertedWith('[E-82] - Tokens not locked for this poll.');
    });

    it('should revert when poll not ended', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '1', { from: OLEG });

      await expect(cvnxGovernance.unlockTokensInPoll(0, { from: OLEG }))
        .to.be.revertedWith('[E-81] - Poll is not ended.');
    });

    it('should emit event TokenUnlock', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '1', { from: OLEG });

      await setCurrentTime(currentTime + 600);
      await cvnxGovernance.unlockTokensInPoll(0, { from: OLEG });

      const oldEvent = await cvnx.getPastEvents('TokenUnlock', { fromBlock: 0, toBlock: 'latest' });
      assert.equal(oldEvent[0].args.tokenOwner, OLEG);
      assert.equal(oldEvent[0].args.amount.toString(), '1');
    });
  });

  describe('stopPoll()', async () => {
    beforeEach(async () => {
      const baseBalance = mock.bn.toWei('200');
      await cvnx.transfer(IVAN, baseBalance);
      await cvnx.transfer(OLEG, baseBalance);
    });

    it('should stop poll', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      const stopTime = currentTime + 10;
      await setCurrentTime(stopTime);
      await cvnxGovernance.stopPoll(0);

      const poll = h.solPollToJsPoll(await cvnxGovernance.polls(0));
      assert.equal(poll.pollStopped, stopTime);
    });

    it('should revert if try to vote after stop', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await setCurrentTime(currentTime + 30);
      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '1', { from: OLEG });

      await setCurrentTime(currentTime + 60);
      await cvnxGovernance.stopPoll(0);

      await setCurrentTime(currentTime + 60);
      await expect(cvnxGovernance.vote(0, mock.voteTypes.FOR, '1', { from: OLEG }))
        .to.be.revertedWith('[E-37] - Poll ended.');
    });

    it('should allow unlock tokens after stop', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await setCurrentTime(currentTime + 30);
      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '1', { from: OLEG });

      await setCurrentTime(currentTime + 31);
      await expect(cvnxGovernance.unlockTokensInPoll(0, { from: OLEG }))
        .to.be.revertedWith('[E-81] - Poll is not ended.');

      await setCurrentTime(currentTime + 60);
      await cvnxGovernance.stopPoll(0);

      await setCurrentTime(currentTime + 60);
      await cvnxGovernance.unlockTokensInPoll(0, { from: OLEG });
    });

    it('should revert if not a poll or contract owner', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, '', { from: IVAN }); // Poll 0

      await setCurrentTime(currentTime + 30);
      await expect(cvnxGovernance.stopPoll(0, { from: OLEG }))
        .to.be.revertedWith('[E-91] - Not a contract or poll owner.');

      await setCurrentTime(currentTime + 60);
      await cvnxGovernance.stopPoll(0, { from: IVAN });
    });
  });

  describe('getPollStatus()', async () => {
    beforeEach(async () => {
      const baseBalance = mock.bn.toWei('200');
      await cvnx.transfer(IVAN, baseBalance);
      await cvnx.transfer(OLEG, baseBalance);
    });

    it('should return valid poll status', async () => {
      const polls = mock.getEventPolls(currentTime);

      // Create polls
      await setCurrentTime(currentTime);
      await cvnxGovernance.createEventPoll(polls[0].pollDeadline, polls[0].pollInfo); // Pool 0
      await cvnxGovernance.createEventPoll(polls[1].pollDeadline, polls[1].pollInfo); // Pool 1
      await cvnxGovernance.createEventPoll(polls[2].pollDeadline, polls[2].pollInfo); // Pool 2

      const votesFor = mock.getVotesTotalFor(OLEG, IVAN);
      for (let i = 0; i < votesFor.length; i++) {
        await cvnxGovernance.vote(0, votesFor[i].voteType, votesFor[i].voteWeight, { from: votesFor[i].from });
      }

      const votesAgainst = mock.getVotesTotalAgainst(OLEG, IVAN);
      for (let i = 0; i < votesAgainst.length; i++) {
        await cvnxGovernance.vote(1, votesAgainst[i].voteType, votesAgainst[i].voteWeight, { from: votesAgainst[i].from });
      }

      const votesDraw = mock.getVotesTotalDraw(OLEG, IVAN);
      for (let i = 0; i < votesDraw.length; i++) {
        await cvnxGovernance.vote(2, votesDraw[i].voteType, votesDraw[i].voteWeight, { from: votesDraw[i].from });
      }

      const pollStatusInfo0Pending = await cvnxGovernance.getPollStatus(0);
      assert.equal(pollStatusInfo0Pending[0].toString(), '0');
      assert.equal(pollStatusInfo0Pending[1].toString(), mock.poolStatus.PENDING);

      await setCurrentTime(currentTime + 1000000);

      const pollStatusInfo0Approved = await cvnxGovernance.getPollStatus(0);
      assert.equal(pollStatusInfo0Approved[0].toString(), '0');
      assert.equal(pollStatusInfo0Approved[1].toString(), mock.poolStatus.APPROVED);

      const pollStatusInfoRejected = await cvnxGovernance.getPollStatus(1);
      assert.equal(pollStatusInfoRejected[0].toString(), '1');
      assert.equal(pollStatusInfoRejected[1].toString(), mock.poolStatus.REJECTED);

      const pollStatusInfoDraw = await cvnxGovernance.getPollStatus(2);
      assert.equal(pollStatusInfoDraw[0].toString(), '2');
      assert.equal(pollStatusInfoDraw[1].toString(), mock.poolStatus.DRAW);
    });
  });

  describe('getPollExpirationTime()', async () => {
    it('should return valid poll deadline timestamp', async () => {
      const deadline = currentTime + 600;
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(deadline, ''); // Poll 0

      const pollDeadline = await cvnxGovernance.getPollExpirationTime(0);
      assert.equal(deadline, pollDeadline.toNumber());
    });
  });

  describe('getPollStopTime()', async () => {
    it('should return valid poll stop timestamp', async () => {
      const deadline = currentTime + 600;
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(deadline, ''); // Poll 0

      const pollStop1 = await cvnxGovernance.getPollStopTime(0);
      assert.equal(deadline, pollStop1.toNumber());

      const stopTimestamp = currentTime + 60;
      await setCurrentTime(stopTimestamp);
      await cvnxGovernance.stopPoll(0);

      const pollStop2 = await cvnxGovernance.getPollStopTime(0);
      assert.equal(stopTimestamp, pollStop2.toNumber());
    });
  });

  describe('getPollHistory()', async () => {
    beforeEach(async () => {
      const baseBalance = mock.bn.toWei('200');
      await cvnx.transfer(IVAN, baseBalance);
      await cvnx.transfer(OLEG, baseBalance);
      await cvnx.transfer(OLGA, baseBalance);
    });

    it('should return polls where address voted', async () => {
      await h.createAllPolls(currentTime, OLEG, IVAN, accounts[5]);

      const votesInPoll0 = mock.getVotesTotalAgainst(OLEG, IVAN);
      const votesInPoll1 = mock.getVotesTotalAgainst(OLEG, OLGA);
      const votesInPoll2 = mock.getVotesTotalAgainst(OLGA, IVAN);

      // Vote in poll
      for (let i = 0; i < votesInPoll0.length; i++) {
        await setCurrentTime(currentTime);
        await cvnxGovernance.vote(0, votesInPoll0[i].voteType, votesInPoll0[i].voteWeight, { from: votesInPoll0[i].from });
      }

      for (let i = 0; i < votesInPoll1.length; i++) {
        await setCurrentTime(currentTime);
        await cvnxGovernance.vote(1, votesInPoll1[i].voteType, votesInPoll1[i].voteWeight, { from: votesInPoll1[i].from });
      }

      for (let i = 0; i < votesInPoll2.length; i++) {
        await setCurrentTime(currentTime);
        await cvnxGovernance.vote(2, votesInPoll2[i].voteType, votesInPoll2[i].voteWeight, { from: votesInPoll2[i].from });
      }

      const pollHisoryOleg = await cvnxGovernance.getPollHistory(OLEG);
      assert.equal(pollHisoryOleg[0], true);
      assert.equal(pollHisoryOleg[1], true);
      assert.equal(pollHisoryOleg[2], false);

      const pollHisoryIvan = await cvnxGovernance.getPollHistory(IVAN);
      assert.equal(pollHisoryIvan[0], true);
      assert.equal(pollHisoryIvan[1], false);
      assert.equal(pollHisoryIvan[2], true);

      const pollHisoryOlga = await cvnxGovernance.getPollHistory(OLGA);
      assert.equal(pollHisoryOlga[0], false);
      assert.equal(pollHisoryOlga[1], true);
      assert.equal(pollHisoryOlga[2], true);
    });
  });

  describe('getIfUserHasVoted()', async () => {
    beforeEach(async () => {
      const baseBalance = mock.bn.toWei('200');
      await cvnx.transfer(IVAN, baseBalance);
    });

    it('should check address voted in poll or not', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      const isVotedBefore = await cvnxGovernance.getIfUserHasVoted(0, IVAN);
      assert.equal(isVotedBefore, false);

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '1', { from: IVAN });

      const isVotedAfter = await cvnxGovernance.getIfUserHasVoted(0, IVAN);
      assert.equal(isVotedAfter, true);
    });
  });

  describe('getLockedAmount()', async () => {
    beforeEach(async () => {
      const baseBalance = mock.bn.toWei('200');
      await cvnx.transfer(IVAN, baseBalance);
    });

    it('should check address voted in poll or not', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '100', { from: IVAN });

      const lockedAmount = await cvnxGovernance.getLockedAmount(IVAN);
      assert.equal(lockedAmount.toString(), '100');
    });
  });

  describe('getPollLockedAmount()', async () => {
    beforeEach(async () => {
      const baseBalance = mock.bn.toWei('200');
      await cvnx.transfer(IVAN, baseBalance);
    });

    it('should check address voted in poll or not', async () => {
      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 1

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '10', { from: IVAN });
      await cvnxGovernance.vote(1, mock.voteTypes.AGAINST, '20', { from: IVAN });

      const lockedAmountPoll0 = await cvnxGovernance.getPollLockedAmount(0, IVAN);
      assert.equal(lockedAmountPoll0.toString(), '10');

      const lockedAmountPoll1 = await cvnxGovernance.getPollLockedAmount(1, IVAN);
      assert.equal(lockedAmountPoll1.toString(), '20');
    });
  });
});
