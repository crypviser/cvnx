const { use, expect } = require('chai');
const { solidity } = require('ethereum-waffle');

use(solidity);
use(require('chai-as-promised')).should();

const CVNX = artifacts.require('CVNX');
const CVNXGovernance = artifacts.require('CVNXGovernance');

const Reverter = require('./helpers/truffle/reverter');
const setCurrentTime = require('./helpers/truffle/ganacheTimeTraveler');

const Mock = require('./mocks/mock');

contract('CVNX', async (accounts) => {
  const reverter = new Reverter(web3);
  const mock = new Mock();

  let cvnx;
  let cvnxGovernance;

  const IVAN = accounts[1];
  const OLEG = accounts[2];

  const currentTime = Math.round((new Date().getTime()) / 1000);

  before(async () => {
    cvnx = await CVNX.new();
    const cvnxGovernanceAddress = await cvnx.cvnxGovernanceContract();
    cvnxGovernance = await CVNXGovernance.at(cvnxGovernanceAddress);

    await reverter.snapshot();
  });

  afterEach(async () => {
    await reverter.revert();
  });

  describe('transfer()', async () => {
    it('should transfer tokens', async () => {
      const tokensToTransfer = mock.bn.toWei('100');
      await cvnx.transfer(IVAN, tokensToTransfer);

      assert.equal((await cvnx.balanceOf(IVAN)).toString(), mock.bn.toWei('100').toString());
    });

    it('should revert if try to transfer locked tokens', async () => {
      const baseBalance = mock.bn.toWei('100');
      await cvnx.transfer(IVAN, baseBalance);

      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, baseBalance, { from: IVAN });

      await expect(cvnx.transfer(OLEG, '1', { from: IVAN }))
        .to.be.revertedWith('[E-61] - Transfer amount exceeds available tokens.');
    });
  });

  describe('transferFrom()', async () => {
    it('should transfer tokens', async () => {
      await cvnx.transfer(IVAN, mock.bn.toWei('100'));

      await cvnx.approve(OLEG, mock.bn.toWei('75'), { from: IVAN });
      await cvnx.transferFrom(IVAN, OLEG, mock.bn.toWei('75'), { from: OLEG });

      assert.equal((await cvnx.balanceOf(IVAN)).toString(), mock.bn.toWei('25').toString());
      assert.equal((await cvnx.balanceOf(OLEG)).toString(), mock.bn.toWei('75').toString());
    });

    it('should revert if try to transfer locked tokens', async () => {
      const baseBalance = mock.bn.toWei('100');
      await cvnx.transfer(IVAN, baseBalance);

      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, baseBalance, { from: IVAN });

      await cvnx.approve(OLEG, '1', { from: IVAN });
      await expect(cvnx.transferFrom(IVAN, OLEG, '1', { from: OLEG }))
        .to.be.revertedWith('[E-61] - Transfer amount exceeds available tokens.');
    });
  });

  describe('lock()', async () => {
    it('should lock tokens', async () => {
      const baseBalance = mock.bn.toWei('200');
      await cvnx.transfer(IVAN, baseBalance);
      await cvnx.transfer(OLEG, baseBalance);

      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '100', { from: IVAN });
      const locked1 = (await cvnx.lockedAmount(IVAN)).toString();
      assert.equal(locked1, '100');

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '50', { from: IVAN });
      const locked2 = (await cvnx.lockedAmount(IVAN)).toString();
      assert.equal(locked2, '150');

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, '75', { from: OLEG });
      const locked3 = (await cvnx.lockedAmount(OLEG)).toString();
      assert.equal(locked3, '75');
    });

    it('should revert if not enough tokens to vote', async () => {
      const baseBalance = mock.bn.toWei('100');
      await cvnx.transfer(IVAN, baseBalance);

      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await cvnxGovernance.vote(0, mock.voteTypes.FOR, baseBalance, { from: IVAN });

      await expect(cvnxGovernance.vote(0, mock.voteTypes.FOR, '1', { from: IVAN }))
        .to.be.revertedWith('[E-42] - Not enough token on account.');
    });

    it('should revert if try to vote with zero weight', async () => {
      const baseBalance = mock.bn.toWei('100');
      await cvnx.transfer(IVAN, baseBalance);

      await setCurrentTime(currentTime);
      await cvnxGovernance.createProposalPoll(currentTime + 600, ''); // Poll 0

      await expect(cvnxGovernance.vote(0, mock.voteTypes.FOR, '0', { from: IVAN }))
        .to.be.revertedWith('[E-41] - The amount to be locked must be greater than zero.');
    });
  });
});
