const CVNX = artifacts.require('CVNX');

module.exports = async (callback) => {
  console.log('Begin deploy!\n');
  try {
    const cvnx = await CVNX.new();
    const cvnxGovernanceAddress = await cvnx.cvnxGovernanceContract();

    console.log('CVNX Token: ', cvnx.address);
    console.log('CVNX Governance: ', cvnxGovernanceAddress);
    console.log('\nDone!');
  } catch (e) {
    console.log(e.message);
  }

  callback();
};
