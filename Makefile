qa:
	@npm run prettier:solidity && truffle run solhint

verify:
# 	truffle run verify CVNX@ --network main
# 	truffle run verify CVNXGovernance@ --network main

verify-testnet:
	truffle run verify CVNX@0x4bdEDaDa54648f584849f98A618B964BC1dEdfA7 --network ropsten
	truffle run verify CVNXGovernance@0x2d1c35AFe55af1716804689Cb4cfe40282bCb29A --network ropsten
